import json
#import 
import os
import time
from math import *
import matplotlib.pyplot as plt
from numpy import *
import numpy as np
import pylab as p
# import matplotlib
# # Force matplotlib to not use any Xwindows backend.
# matplotlib.use('Agg')
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np



Data_Temp_Hum = [{'Data_Temp_Hum':[]}]
Data_Prediction = [{'Data_Prediction':[]}]
#today = datetime.date.today()

# ----------------***********************----------------------------------
def save_config_interpolation(PuntosX, PuntosY, PosicionX_Y_NODOA, PosicionX_Y_NODOB, PosicionX_Y_NODOC):
    
    try:
        print "Escribiendo Archivo Json de configuracion"
        file_write = open('Config/config','w')
        file_write.write(PuntosX+'\n')
        file_write.write(PuntosY+'\n')
        file_write.write(PosicionX_Y_NODOA+'\n')
        file_write.write(PosicionX_Y_NODOB+'\n')
        file_write.write(PosicionX_Y_NODOC+'\n')
        file_write.close()
        print "Se ha escrito el archivo  de configuracioncorrectamente"
        
        return True
    except:
        print "Error al escribir el archivo  de configuracion"
        return False
        pass

    
# ----------------***********************----------------------------------
def save_info_json(NODO_in, Temp_in, Hum_in):
    data_json = {
        'NODO':NODO_in,
        'Temp':Temp_in,
        'Hum':Hum_in,
        'Date_Time':time.strftime("%d/%m/%y")+" "+time.strftime("%H:%M")
        }

    Data_Temp_Hum[0]['Data_Temp_Hum'].append(data_json)

    with open('Reportes/Data_Temp_Hum_'+time.strftime("%d_%m_%y_")+time.strftime("%H_%M")+'.json','w') as json_file:
        try:
            print "Escribiendo Archivo Json de informacion "
            json.dump(Data_Temp_Hum, json_file)
            print "Se ha escrito el archivo JSON de informacion correctamente"
        except:
            print "Error al escribir el archivo JSON de informacion"

# ----------------***********************----------------------------------
def save_interpolation_json(x,y, Temp_predic, Hum_predic,how,timeIN):
    position = str(x) + "," + str(y)
    data_json = {
        'position':position,
        'Temp':Temp_predic,
        'Hum':Hum_predic,
        'Date_Time':time.strftime("%d/%m/%y")+" "+time.strftime("%H:%M"),
        'how':how
        }

    Data_Prediction[0]['Data_Prediction'].append(data_json)

    with open('Interpo_Reportes/Data_Interpolacion_'+time.strftime("%d_%m_%y_")+timeIN+'.json','w') as json_file:
        try:
            #print "Escribiendo Archivo Json de interpolacion "
            json.dump(Data_Prediction, json_file)
            #print "Se ha escrito el archivo JSON de la prediccion correctamente"
        except:
            print "Error al escribir el archivo JSON de la prediccion"

# ----------------***********************----------------------------------
def clean_data(clean_data_in,delete1,delete2):
	data_return = clean_data_in.strip(delete1)
	data_return = data_return.strip(delete2)

	return data_return
# ----------------***********************----------------------------------
def clean_data2(clean_data_in,delete1):
	data_return = clean_data_in.strip(delete1)

	return data_return
# ----------------***********************----------------------------------
def get_temp_hum(array_in):
    nodo = array_in[0]
    tem_hum_array = array_in[1].split(' ')
    
    temperature = clean_data(tem_hum_array[1],'<','C')
    
    try:
        Temp = float(temperature)
    except ValueError:
        print "ERROR CONVERT!!!"
        Temp = float(0)
    
    #for t in temperature:
        #if t in "0123456789.":
            #bandera0 = True
    #if bandera0:
        #Temp = float(temperature)
    #else:
        #Temp = float(0)
        
    
    humidity = clean_data(tem_hum_array[4],';','%RH')
    
    try:
        Hum = float(humidity)
    except ValueError:
        print "ERROR CONVERT!!!"
        Hum = float(0)
    
    #for h in humidity:
        #if h in "0123456789.":
            #bandera = True
    #if bandera:
        #Hum = float(humidity)
    #else:
        #Hum = float(0)

    print "NODO:>", nodo, "TEMP AND HUM", tem_hum_array
    print "TEMP:",Temp
    print "HUM:",Hum
    save_info_json(nodo,Temp,Hum)

    return nodo,Temp,Hum

# ----------------***********************----------------------------------
def get_temp_hum_prom(NODO,timeIN,fecha_in):

    
    temp = 0 
    hum = 0
    print NODO
    #with open('Reportes/Data_Temp_Hum.json') as read_file:
    #with open('Reportes/Data_Temp_Hum_'+time.strftime("%d_%m_%y_")+timeIN+'.json') as read_file:
    with open('Reportes/Data_Temp_Hum_'+fecha_in+timeIN+'.json') as read_file:
        print 'Reportes/Data_Temp_Hum_'+fecha_in+timeIN+'.json'
        data = json.load(read_file)

    print data[0]['Data_Temp_Hum']

    for i in range(len(data[0]['Data_Temp_Hum'])):
        if data[0]['Data_Temp_Hum'][i]['NODO'] == NODO:
            temp = data[0]['Data_Temp_Hum'][i]['Temp']
            hum = data[0]['Data_Temp_Hum'][i]['Hum']
            break
        else:
            print "Buscando...."

    #temp=0
    #hum = 0
    return temp, hum


# ----------------***********************----------------------------------
def get_distance(x1,y1,x2,y2):
    d = sqrt(pow((x2-x1),2)+pow((y2-y1),2))
    return d

# ----------------***********************---------------------------------
def read_config():
    
    read_file = []
    
    file_r = open('Config/config','r')
    for l in file_r.readlines():
        print l
        read_file.append(l)
    file_r.close()
    
    if len(read_file) == 0:
        Px = ''
        Py = ''
        PNA = ''
        PNB = ''
        PNC= '' 
        read_ans = False
    else:
        Px = read_file[0]
        Py = read_file[1]
        PNA = read_file[2].split(',')
        PNB = read_file[3].split(',')
        PNC= read_file[4].split(',')
        read_ans = True
    
    print "Respuesta de lectura del archivo de configuracion: ",Px, Py, PNA, PNB, PNC, read_ans 
    return Px, Py, PNA, PNB, PNC, read_ans 
# ----------------***********************----------------------------------
def convert_to_int_array(arrayIn):
    
    for jj in range(len(arrayIn)):
        arrayIn[jj] = int(arrayIn[jj])
        
    return arrayIn
    
# ----------------***********************----------------------------------
def make_interpolation(TimeIN,tiempo_tran):
    
    puntos_X,puntos_Y,position_NODOA,position_NODOB,position_NODOC, read_ansFile = read_config()
    
    path='Reportes'
    lstDir = os.listdir(path)
    print lstDir


    
    fecha = time.strftime("%d_%m_%y_")
    fecha_title = time.strftime("%d/%m/%y %H:%M")
    # fecha = '16_03_17_'
    # fecha_title = '16/03/17 10:00'
    
    
    #if read_ansFile and ('Data_Temp_Hum_'+time.strftime("%d_%m_%y_")+TimeIN+'.json' in lstDir):
    if read_ansFile and ('Data_Temp_Hum_'+ fecha +TimeIN+'.json' in lstDir):
    
        print "make_interpolation"
        #puntos_X = 7
        #puntos_Y = 8
        print "Aqui"
        
        
        puntos_X = int(clean_data2(puntos_X,'\n'))
        puntos_Y = int(clean_data2(puntos_Y,'\n'))

        #position_NODOA = [0,0]
        #position_NODOB = [7,4]
        #position_NODOC = [0,8]
        
        position_NODOA[1] = clean_data2(position_NODOA[1],'\n')
        position_NODOB[1] = clean_data2(position_NODOB[1],'\n')
        position_NODOC[1] = clean_data2(position_NODOC[1],'\n')
        
        
        position_NODOA = convert_to_int_array(position_NODOA)
        position_NODOB = convert_to_int_array(position_NODOB)
        position_NODOC = convert_to_int_array(position_NODOC)
        

        matriz_temp = [range(puntos_X) for i in range(puntos_Y)]
        matriz_hum = [range(puntos_X) for i in range(puntos_Y)]

        #print matriz_temp
        #print matriz_hum

        temp_NODOA,hum_NODOA = get_temp_hum_prom('N0DO A',TimeIN,fecha)
        temp_NODOB,hum_NODOB = get_temp_hum_prom('N0DO B',TimeIN,fecha)
        temp_NODOC,hum_NODOC = get_temp_hum_prom('N0DO C',TimeIN,fecha)

        #print temp_NODOA,hum_NODOA
        #print temp_NODOB,hum_NODOB
        #print temp_NODOC,hum_NODOC

        for i in range(puntos_Y):
            for j in range(puntos_X):
                #print i,j

                if position_NODOA[0] == i and position_NODOA[1] == j:
                    #print "Aqui nodo A"
                    matriz_temp[i][j] = temp_NODOA
                    matriz_hum[i][j] = hum_NODOA

                elif position_NODOB[0] == i and position_NODOB[1] == j:
                    #print "Aqui nodo B"
                    matriz_temp[i][j] = temp_NODOB
                    matriz_hum[i][j] = hum_NODOB

                elif position_NODOC[0] == i and position_NODOC[1] == j:
                    #print "Aqui nodo C"
                    matriz_temp[i][j] = temp_NODOC
                    matriz_hum[i][j] = hum_NODOC

                else:
                    distance_to_NODOA = get_distance(i,j,position_NODOA[0],position_NODOA[1])
                    distance_to_NODOB = get_distance(i,j,position_NODOB[0],position_NODOB[1])
                    distance_to_NODOC = get_distance(i,j,position_NODOC[0],position_NODOC[1])

                    """
                    temp_prediction = ((temp_NODOA/distance_to_NODOA)+(temp_NODOB/distance_to_NODOB)+(temp_NODOC/distance_to_NODOC))/((1/distance_to_NODOA)+(1/distance_to_NODOB)+(1/distance_to_NODOC))

                    hum_prediction = ((hum_NODOA/distance_to_NODOA)+(hum_NODOB/distance_to_NODOB)+(hum_NODOC/distance_to_NODOC))/((1/distance_to_NODOA)+(1/distance_to_NODOB)+(1/distance_to_NODOC))
                    """

                    temp_prediction = ((temp_NODOA/distance_to_NODOA)+(temp_NODOB/distance_to_NODOB)+(temp_NODOC/distance_to_NODOC))/((1/pow(distance_to_NODOA,2))+(1/pow(distance_to_NODOB,2))+(1/pow(distance_to_NODOC,2)))

                    hum_prediction = ((hum_NODOA/distance_to_NODOA)+(hum_NODOB/distance_to_NODOB)+(hum_NODOC/distance_to_NODOC))/((1/pow(distance_to_NODOA,2))+(1/pow(distance_to_NODOB,2))+(1/pow(distance_to_NODOC,2)))

                    matriz_temp[i][j] = temp_prediction
                    matriz_hum[i][j] = hum_prediction

                    save_interpolation_json(i,j,temp_prediction,hum_prediction,'prediction',TimeIN)

                """
                    temp_prediction1 = ((temp_NODOA/distance_to_NODOA)+(temp_NODOB/distance_to_NODOB)+(temp_NODOC/distance_to_NODOC))/((1/pow(distance_to_NODOA,2))+(1/pow(distance_to_NODOB,2))+(1/pow(distance_to_NODOC,2)))

                    hum_prediction1 = ((hum_NODOA/distance_to_NODOA)+(hum_NODOB/distance_to_NODOB)+(hum_NODOC/distance_to_NODOC))/((1/pow(distance_to_NODOA,2))+(1/pow(distance_to_NODOB,2))+(1/pow(distance_to_NODOC,2)))

                    save_interpolation_json(i,j,temp_prediction1,hum_prediction1,'prediction1')

                """
        #plt.contourf(matriz_temp)
        # plt.figure(1)
        plt.grid(True)
        plt.imshow(matriz_temp, cmap='viridis', interpolation='nearest')
        plt.colorbar()
        plt.title('Interpolacion de temperatura '+ fecha_title)
        #plt.savefig('Interpo_Images/inter_temp_'+time.strftime("%d_%m_%y_")+TimeIN+'.png')
        plt.savefig('Interpo_Images/inter_temp_'+fecha+TimeIN+'.png')
        plt.savefig('static/img/'+'temp_inter1'+tiempo_tran+'.png')
        print "Guardando Imagen de interpolacion temperatura"
        plt.clf()
        # plt.show()


        # plt.figure(2)
        plt.grid(True)
        plt.imshow(matriz_hum,cmap=plt.cm.autumn, interpolation='bilinear')
        plt.colorbar()
        plt.title('Interpolacion de humedad '+ fecha_title)
        # plt.draw()
        #plt.savefig('Interpo_Images/inter_humi_'+time.strftime("%d_%m_%y_")+TimeIN+'.png')
        plt.savefig('Interpo_Images/inter_humi_'+fecha+TimeIN+'.png')
        plt.savefig('static/img/'+'humi_inter1'+tiempo_tran+'.png')
        print "Guardando Imagen de interpolacion temperatura"
        plt.clf()   #Clear the figure for the next loop
        # plt.show()
        
        
                
        

    else:
        print 'Aun no se puede hacer ninguna interpolacion'
    
