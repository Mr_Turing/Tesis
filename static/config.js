

$(document).ready(function() {
            // Use a "/test" namespace.
            // An application can open a connection on multiple namespaces, and
            // Socket.IO will multiplex all those connections on a single
            // physical channel. If you don't care about multiple channels, you
            // can set the namespace to an empty string.
            namespace = '/tesis';
            // Connect to the Socket.IO server.
            // The connection URL has the following format:
            //     http[s]://<domain>:<port>[/<namespace>]
            var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port + namespace);
            // Event handler for new connections.
            // The callback function is invoked when a connection with the
            // server is established.
           
            //alert('HOLA mundo');
            socket.on('connect', function() {
            socket.emit('my_event', {data: 'Yo soy el Cliente, estoy conectado'});
                });
            // Event handler for server sent data.
            // The callback function is invoked whenever the server emits data
            // to the client. The data is then displayed in the "Received"
            // section of the page.
            socket.on('my_response', function(msg) {
                // alert(msg.data);
                $('#example').text(msg.data).html();
                //$('#log').append('<br>' + $('<div/>').text('Received #' + msg.count + ': ' + msg.data).html());
            });


            $('form#emit').submit(function(event) {
                socket.emit('my_event', {data: $('#emit_data').val()});
                return false;
            });
      
            
            $("#xA").change(function(){
                console.log($('#xA').val());
                if(parseInt($('#xA').val()) > parseInt($('#pX').val())){
                alert("Corrige la posicion X del NODO A supera el numero de puntos en el eje X");
                }
                else{
                    console.log("OK");
                }
            });
            
            $("#xB").change(function(){
                console.log($('#xB').val());
                if(parseInt($('#xB').val()) > parseInt($('#pX').val())){
                alert("Corrige la posicion X del NODO B supera el numero de puntos en el eje X");
                }
                else{
                    console.log("OK");
                }
            });
            
            $("#xC").change(function(){
                console.log($('#xC').val());
                if(parseInt($('#xC').val()) > parseInt($('#pX').val())){
                alert("Corrige la posicion X del NODO C supera el numero de puntos en el eje X");
                }
                else{
                    console.log("OK");
                }
            });
            
            $("#yA").change(function(){
                console.log($('#yA').val());
                if(parseInt($('#yA').val()) > parseInt($('#pY').val())){
                alert("Corrige la posicion Y del NODO A supera el numero de puntos en el eje Y");
                }
                else{
                    console.log("OK");
                }
            });
            
            $("#yB").change(function(){
                console.log($('#yB').val());
                if(parseInt($('#yB').val()) > parseInt($('#pY').val())){
                alert("Corrige la posicion Y del NODO B supera el numero de puntos en el eje Y");
                }
                else{
                    console.log("OK");
                }
            });
            
            $("#yC").change(function(){
                console.log($('#yC').val());
                if(parseInt($('#yC').val()) > parseInt($('#pY').val())){
                alert("Corrige la posicion Y del NODO C supera el numero de puntos en el eje Y");
                }
                else{
                    console.log("OK");
                }
            });
            
            $("#emitConfig").click(function(){                
                    
                    if(parseInt($('#xA').val()) > parseInt($('#pX').val()) || parseInt($('#xB').val()) > parseInt($('#pX').val()) || parseInt($('#xC').val()) > parseInt($('#pX').val()) || parseInt($('#yA').val()) > parseInt($('#pY').val()) || parseInt($('#yB').val()) > parseInt($('#pY').val()) || parseInt($('#yC').val()) > parseInt($('#pY').val()) ){
                        alert("Corrige los valores que superan los puntos de cada eje.")
                        return false;
                    }
                    else{
                        console.log("Vamos a enviar");
                        socket.emit('my_config', {
                            pX: $('#pX').val(),
                            pY: $('#pY').val(),
                            xA: $('#xA').val(),
                            xB: $('#xB').val(),
                            xC: $('#xC').val(),
                            yA: $('#yA').val(),                            
                            yB: $('#yB').val(),
                            yC: $('#yC').val()
                            
                        });
                        
                    }
                
            });
            
            socket.on('save_respon', function(msg) {
                // alert(msg.data);
//                 $('#example').text(msg.data).html();
                alert(msg.data);
                //$('#log').append('<br>' + $('<div/>').text('Received #' + msg.count + ': ' + msg.data).html());
            });
            

});
