
function get_charts_NODOA(socket) {



   socket.on('temp_hum_NODOA', function(msg){
 		//alert( msg.temperature);
		
		$(document).data('temperaturaA',msg.temperatureA);
		//alert(mensaje);
		$(document).data('humedadA',msg.humidityA);
        
        var tempA = $(document).data('temperaturaA');
        var humA = $(document).data('humedadA');
        $('#TA').html(tempA + "&deg C");
        $('#HA').html(humA + "&#37");
	});


    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    // Create the chart
    Highcharts.stockChart('container_NODOA', {
        chart: {
            events: {
                load: function () {

                    // set up the updating of the chart each second
                    var series = this.series[0];
                    var series1 = this.series[1];
		    setInterval(function () {
                        var x = (new Date()).getTime(), // current time
                            //y = Math.round(Math.random() * 100);
                            y = $(document).data('temperaturaA');
			series.addPoint([x, y], true, true);
			
			var y1 = $(document).data('humedadA');
			series1.addPoint([x, y1], true, true);


                    }, 1000);

                }
            }
        },

        rangeSelector: {
            buttons: [{
                count: 1,
                type: 'minute',
                text: '1M'
            }, {
                count: 5,
                type: 'minute',
                text: '5M'
            }, {
                type: 'all',
                text: 'All'
            }],
            inputEnabled: false,
            selected: 0
        },

        /*title: {
            text: 'Temperatura y humedad Nodo A'
        },*/

        exporting: {
            enabled: false
        },

        series: [{
            name: 'Temperatura Nodo A',
            data: (function () {
                // generate an array of random data
                var data = [],
                    time = (new Date()).getTime(),
                    i;

                for (i = -999; i <= 0; i += 1) {
                    data.push([
                        time + i * 1000,
                        $(document).data('temperaturaA')
			//Math.round(Math.random() * 100)
                    ]);
                }
                return data;
            }())
        },
	{
	    name:'Humedad Nodo A',
	    data: (function(){
	    	var data1 = [],
		    time = (new Date()).getTime(),
		    i;
		for(i = -999; i <= 0; i += 1){
		    data1.push([
		    	time + i * 1000,
  			$(document).data('humedadA')
                    ]);
		}
		return data1
	    }())
	}]
    });

};



////NODO B
function get_charts_NODOB(socket) {



   socket.on('temp_hum_NODOB', function(msg){
 		//alert( msg.temperature);
		
		$(document).data('temperaturaB',msg.temperatureB);
		//alert(mensaje);
		$(document).data('humedadB',msg.humidityB);
        
        var tempB = $(document).data('temperaturaB');
        var humB = $(document).data('humedadB');
        $('#TB').html(tempB + "&deg C");
        $('#HB').html(humB + "&#37");
        
	});


    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    // Create the chart
    Highcharts.stockChart('container_NODOB', {
        chart: {
            events: {
                load: function () {

                    // set up the updating of the chart each second
                    var series = this.series[0];
                    var series1 = this.series[1];
		    setInterval(function () {
                        var x = (new Date()).getTime(), // current time
                            //y = Math.round(Math.random() * 100);
                            y = $(document).data('temperaturaB');
			series.addPoint([x, y], true, true);
			
			var y1 = $(document).data('humedadB');
			series1.addPoint([x, y1], true, true);


                    }, 1000);

                }
            }
        },

        rangeSelector: {
            buttons: [{
                count: 1,
                type: 'minute',
                text: '1M'
            }, {
                count: 5,
                type: 'minute',
                text: '5M'
            }, {
                type: 'all',
                text: 'All'
            }],
            inputEnabled: false,
            selected: 0
        },

        /*title: {
            text: 'Temperatura y humedad Nodo B'
        },*/

        exporting: {
            enabled: false
        },

        series: [{
            name: 'Temperatura Nodo B',
            data: (function () {
                // generate an array of random data
                var data = [],
                    time = (new Date()).getTime(),
                    i;

                for (i = -999; i <= 0; i += 1) {
                    data.push([
                        time + i * 1000,
                        $(document).data('temperaturaB')
			//Math.round(Math.random() * 100)
                    ]);
                }
                return data;
            }())
        },
	{
	    name:'Humedad Nodo B',
	    data: (function(){
	    	var data1 = [],
		    time = (new Date()).getTime(),
		    i;
		for(i = -999; i <= 0; i += 1){
		    data1.push([
		    	time + i * 1000,
  			$(document).data('humedadB')
                    ]);
		}
		return data1
	    }())
	}]
    });

};





///NODO C
function get_charts_NODOC(socket) {



   socket.on('temp_hum_NODOC', function(msg){
 		//alert( msg.temperature);
		
		$(document).data('temperaturaC',msg.temperatureC);
		//alert(mensaje);
		$(document).data('humedadC',msg.humidityC);
        
        var tempC = $(document).data('temperaturaC');
        var humC = $(document).data('humedadC');
        $('#TC').html(tempC + "&deg C");
        $('#HC').html(humC + "&#37");
	});


    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    // Create the chart
    Highcharts.stockChart('container_NODOC', {
        chart: {
            events: {
                load: function () {

                    // set up the updating of the chart each second
                    var series = this.series[0];
                    var series1 = this.series[1];
		    setInterval(function () {
                        var x = (new Date()).getTime(), // current time
                            //y = Math.round(Math.random() * 100);
                            y = $(document).data('temperaturaC');
			series.addPoint([x, y], true, true);
			
			var y1 = $(document).data('humedadC');
			series1.addPoint([x, y1], true, true);


                    }, 1000);

                }
            }
        },

        rangeSelector: {
            buttons: [{
                count: 1,
                type: 'minute',
                text: '1M'
            }, {
                count: 5,
                type: 'minute',
                text: '5M'
            }, {
                type: 'all',
                text: 'All'
            }],
            inputEnabled: false,
            selected: 0
        },

        /*title: {
            text: 'Temperatura y humedad Nodo C'
        },*/

        exporting: {
            enabled: false
        },

        series: [{
            name: 'Temperatura Nodo C',
            data: (function () {
                // generate an array of random data
                var data = [],
                    time = (new Date()).getTime(),
                    i;

                for (i = -999; i <= 0; i += 1) {
                    data.push([
                        time + i * 1000,
                        $(document).data('temperaturaC')
			//Math.round(Math.random() * 100)
                    ]);
                }
                return data;
            }())
        },
	{
	    name:'Humedad Nodo C',
	    data: (function(){
	    	var data1 = [],
		    time = (new Date()).getTime(),
		    i;
		for(i = -999; i <= 0; i += 1){
		    data1.push([
		    	time + i * 1000,
  			$(document).data('humedadC')
                    ]);
		}
		return data1
	    }())
	}]
    });

};






$(document).ready(function() {
            // Use a "/test" namespace.
            // An application can open a connection on multiple namespaces, and
            // Socket.IO will multiplex all those connections on a single
            // physical channel. If you don't care about multiple channels, you
            // can set the namespace to an empty string.
            namespace = '/tesis';
            // Connect to the Socket.IO server.
            // The connection URL has the following format:
            //     http[s]://<domain>:<port>[/<namespace>]
            var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port + namespace);
            // Event handler for new connections.
            // The callback function is invoked when a connection with the
            // server is established.
           
	    //alert('HOLA mundo');
	    socket.on('connect', function() {
		socket.emit('my_event', {data: 'Yo soy el Cliente, estoy conectado'});
            });
            // Event handler for server sent data.
            // The callback function is invoked whenever the server emits data
            // to the client. The data is then displayed in the "Received"
            // section of the page.
            socket.on('my_response', function(msg) {
                // alert(msg.data);
		$('#example').text(msg.data).html();
		//$('#log').append('<br>' + $('<div/>').text('Received #' + msg.count + ': ' + msg.data).html());
            });


	    get_charts_NODOA(socket);
	    get_charts_NODOB(socket);
	    get_charts_NODOC(socket);




            // Interval function that tests message latency by sending a "ping"
            // message. The server then responds with a "pong" message and the
            // round trip time is measured.
            var ping_pong_times = [];
            var start_time;
            window.setInterval(function() {
                start_time = (new Date).getTime();
                socket.emit('my_ping');
            }, 1000);
            // Handler for the "pong" message. When the pong is received, the
            // time from the ping is stored, and the average of the last 30
            // samples is average and displayed.
            socket.on('my_pong', function() {
                var latency = (new Date).getTime() - start_time;
                ping_pong_times.push(latency);
                ping_pong_times = ping_pong_times.slice(-30); // keep last 30 samples
                var sum = 0;
                for (var i = 0; i < ping_pong_times.length; i++)
                    sum += ping_pong_times[i];
                $('#ping-pong').text(Math.round(10 * sum / ping_pong_times.length) / 10);
            });
            // Handlers for the different forms in the page.
            // These accept data from the user and send it to the server in a
            // variety of ways
            $('form#emit').submit(function(event) {
                socket.emit('my_event', {data: $('#emit_data').val()});
                return false;
            });
            $('form#broadcast').submit(function(event) {
                socket.emit('my_broadcast_event', {data: $('#broadcast_data').val()});
                return false;
            });
            $('form#join').submit(function(event) {
                socket.emit('join', {room: $('#join_room').val()});
                return false;
            });
            $('form#leave').submit(function(event) {
                socket.emit('leave', {room: $('#leave_room').val()});
                return false;
            });
            $('form#send_room').submit(function(event) {
                socket.emit('my_room_event', {room: $('#room_name').val(), data: $('#room_data').val()});
                return false;
            });
            $('form#close').submit(function(event) {
                socket.emit('close_room', {room: $('#close_room').val()});
                return false;
            });
            $('form#disconnect').submit(function(event) {
                socket.emit('disconnect_request');
                return false;
            });
            



});
