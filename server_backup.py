from flask import Flask, render_template
from flask_socketio import SocketIO, Namespace, emit
import random
import serial

async_mode = None


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread = None

def clean_data(clean_data_in,delete1,delete2):
	data_return = clean_data_in.strip(delete1)
	data_return = data_return.strip(delete2)
	
	return data_return

def save_data(array_in):

	nodo = array_in[0]
	tem_hum_array = array_in[1].split(' ')

	Temp = float(clean_data(tem_hum_array[1],'<','C'))
	Hum = float(clean_data(tem_hum_array[4],';','%RH'))


	print "NODO:>", nodo, "TEMP AND HUM", tem_hum_array
	print "TEMP:",Temp
	print "HUM:",Hum

	return nodo,Temp,Hum

def temperature_thread():

	device_xbee = serial.Serial("/dev/ttyUSB0",timeout=1)

	while True:
		#temperature = random.randrange(100)
		#humidity = random.randrange(100)
		

		temp_and_hum = device_xbee.readlines()

		if len(temp_and_hum) != 0:
			print ">>> Bandera 1"
			array_tem_hum = temp_and_hum[0].split('\r')
			print ">>> Out Bandera1 >>>", array_tem_hum, len(array_tem_hum)
			if len(array_tem_hum) == 5:
				print ">>> Bandera 2"
				nodo,tem,hum = save_data(array_tem_hum)

		#print "Lectura del dispositivo de cordinacion",temp_and_hum

				temperature = tem
				humidity = hum

				if nodo == 'N0DO A':
					socketio.emit('temp_hum_NODOA',
						{'temperatureA':temperature,'humidityA':humidity},
				 			namespace='/tesis')

				if nodo == 'N0DO B':
					socketio.emit('temp_hum_NODOB',
                                		{'temperatureB':temperature,'humidityB':humidity},
                                 			namespace='/tesis')

				if nodo == 'N0DO C':
					socketio.emit('temp_hum_NODOC',
                                		{'temperatureC':temperature,'humidityC':humidity},
                                 			namespace='/tesis')

				print "Dato enviado al ",nodo, 'con una temperatura de ', temperature, 'y una humedad de ', humidity
		else:
			print "El dispositivo no se esta leyendo en este momento"

		socketio.sleep(0.5)


@app.route("/")
def hello():
	return render_template('index.html', async_mode = socketio.async_mode)


@socketio.on('connect', namespace = '/tesis')
def connect():
	global thread
	if thread is None:
		thread = socketio.start_background_task(target=temperature_thread)
	emit('my_response', {'data':'Connect ECCI'})
	#print "Connect ECCI desde el servidor"
	


@socketio.on('my_event', namespace = '/tesis')
def my_event(message):
	print message['data']

if __name__ == '__main__':
	socketio.run(app,host='0.0.0.0',debug=True,port=3005)
