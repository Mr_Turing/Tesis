from flask import Flask, render_template
from flask_socketio import SocketIO, Namespace, emit
import random
import serial
from tesis_interpolacion import *
from datetime import datetime, timedelta
import os

import eventlet
eventlet.monkey_patch()

async_mode = None


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread = None



def temperature_thread():

    os.system('sudo chmod 666 /dev/ttyUSB0')

    device_xbee = serial.Serial("/dev/ttyUSB0",timeout=1)
    band = True

    while True:
        
        if band:
            ahoraI = datetime.now()
            mas10I = ahoraI + timedelta(seconds=600)
            
            ahora = datetime.now()
            mas10 = ahora + timedelta(seconds=600)
            
            ahora = ahora.time()
            
            ahora = ahora.strftime("%H_%M")  
            mas10 = mas10.strftime("%H_%M") 
            
            #print 'ahora '+ahora
            #print '+10 '+mas10
            
            band = False
            
        ahoranew = datetime.now().strftime("%H_%M")            
        print 'ahoranew',ahoranew
        print mas10
        
        if ahoranew == mas10:
            time_interpolacion1 = (ahoraI + timedelta(seconds=60)).strftime("%H_%M") 
            time_interpolacion2 = (ahoraI + timedelta(seconds=300)).strftime("%H_%M") 
            time_interpolacion3 = (mas10I - timedelta(seconds=60)).strftime("%H_%M") 
            #print "debes hacer la interpolacion YA!"
            #print "-------------*******-----------------"
            #print "time_interpolacion1", time_interpolacion1
            #print "time_interpolacion2", time_interpolacion2
            #print "time_interpolacion3", time_interpolacion3
            #print "-------------*******-----------------"
            
            make_interpolation(time_interpolacion1,"ah")
            make_interpolation(time_interpolacion2,"m5")
            make_interpolation(time_interpolacion3,"m9")

            band = True


        
        #temperature = random.randrange(100)
        #humidity = random.randrange(100)

        temp_and_hum = device_xbee.readlines()
       
        if len(temp_and_hum) != 0:
            print ">>> Bandera 1"
            array_tem_hum = temp_and_hum[0].split('\r')
            print ">>> Out Bandera1 >>>", array_tem_hum, len(array_tem_hum)
            if len(array_tem_hum) == 5:
                print ">>> Bandera 2"
                nodo,tem,hum = get_temp_hum(array_tem_hum)

        #print "Lectura del dispositivo de cordinacion",temp_and_hum

                temperature = tem
                humidity = hum
                if nodo == 'N0DO A':
                    socketio.emit('temp_hum_NODOA',
                                  {'temperatureA':temperature,'humidityA':humidity},
                                namespace='/tesis')
                    
                if nodo == 'N0DO B':
                    socketio.emit('temp_hum_NODOB',                             
                                  {'temperatureB':temperature,'humidityB':humidity},
                                            namespace='/tesis')

                if nodo == 'N0DO C':
                    socketio.emit('temp_hum_NODOC',                                
                                  {'temperatureC':temperature,'humidityC':humidity},
                                            namespace='/tesis')

                print "Dato enviado al ",nodo, 'con una temperatura de ', temperature, 'y una humedad de ', humidity
        else:
            print "El dispositivo no se esta leyendo en este momento"
        socketio.sleep(0.5)
        
        

@app.route("/")
@app.route("/dashboard")
def dashboard():
    return render_template('dashboard.html', async_mode = socketio.async_mode)
    #return render_template('index.html', async_mode = socketio.async_mode)


@app.route("/interpolacion")
def interpolacion():
    return render_template('interpolacion.html', async_mode = socketio.async_mode)

@app.route("/maps")
def maps():
    return render_template('maps.html', async_mode = socketio.async_mode)
    
@app.route("/config")
def config():
    return render_template('configuracion.html', async_mode = socketio.async_mode)
    
@app.route("/notificaciones")
def notificaciones():
    return render_template('notificaciones.html', async_mode = socketio.async_mode)



@socketio.on('connect', namespace = '/tesis')
def connect():
	global thread
	if thread is None:
		thread = socketio.start_background_task(target=temperature_thread)
	emit('my_response', {'data':'Connect ECCI'})
	#print "Connect ECCI desde el servidor"
	


@socketio.on('my_event', namespace = '/tesis')
def my_event(message):
	print message['data']
	
	
@socketio.on('my_config', namespace = '/tesis')
def my_config(message):
    print message
    puntosX = message['pX']
    puntosY = message['pY']
    posX_Y_NA = message['xA'] + ',' + message['yA']
    posX_Y_NB = message['xB'] + ',' + message['yB']
    posX_Y_NC = message['xC'] + ',' + message['yC']
    ans = save_config_interpolation(puntosX, puntosY, posX_Y_NA, posX_Y_NB, posX_Y_NC)

    if ans == True:
        emit('save_respon', {'data':'Configuracion guardada.'})
    else:
        emit('save_respon', {'data':'Error al guardar la configuracion.'})


if __name__ == '__main__':
    #socketio.run(app,host='0.0.0.0')
	socketio.run(app,host='0.0.0.0',debug=True,port=3005)
